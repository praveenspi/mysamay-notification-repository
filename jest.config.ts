import type { Config } from '@jest/types';

// Sync object
const config: Config.InitialOptions = {
    verbose: false,
    moduleFileExtensions: [
        "js",
        "json",
        "ts"
    ],
    rootDir: "src",
    testRegex: ".spec.ts$",
    transform: {
        "^.+\\.(t)s$": "ts-jest"
    },
    collectCoverage: true,
    collectCoverageFrom: [
        "./**/*.ts"
    ],
    coverageDirectory: "../coverage",
    testEnvironment: "node",
    coverageThreshold: {
        global: {
            statements: 10
        }
    }
};
export default config;
