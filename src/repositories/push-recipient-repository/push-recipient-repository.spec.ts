import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';





import { PushRecipientRepository } from "./push-recipient-repository";


describe('PushRepository', () => {
    let provider: PushRecipientRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [PushRecipientRepository],
        }).compile();

        provider = module.get(PushRecipientRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
