import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';

import { PushRecipient } from "@neb-sports/mysamay-notification-model";

@Injectable()
export class PushRecipientRepository {
    db: Db;
    collection: Collection<PushRecipient>;

    constructor(private dbProvider: MongoProvider) {}

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(PushRecipient.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createPushRecipient(push: PushRecipient): Promise<PushRecipient> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(push);
            if (result.insertedCount === 1) return result.ops[0];
            return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getRecipentByAnyQuery(
        query: FilterQuery<PushRecipient>,
        projection: SchemaMember<PushRecipient, any>,
    ): Promise<PushRecipient> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countRecipientByAnyQuery(
        query: FilterQuery<PushRecipient>,
    ): Promise<number> {
        try {
            await this.getCollection();
            return await this.collection.countDocuments(query);
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getRecipientByAnyQueryPaginated(
        query: FilterQuery<PushRecipient>,
        skip: number,
        limit: number,
        sort: SortOptionObject<PushRecipient>,
        projection: SchemaMember<PushRecipient, any>,
    ): Promise<PushRecipient[]> {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .skip(skip)
                .limit(limit)
                .sort(sort)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
    }

    async getLastTenRecipientByAnyQuery(
        query: FilterQuery<PushRecipient>,
        projection: SchemaMember<PushRecipient, any>,
    ): Promise<PushRecipient[]> {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .sort({timestamp:-1})
                .limit(10)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
    }

    async bulkInsert(pushList: PushRecipient[]) {
        await this.getCollection();
        let bulk = this.collection.initializeUnorderedBulkOp();
        for(let push of pushList) {
            bulk.insert(push);
        }
        let result = await bulk.execute();
        return result;
    }

   
}
