import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';

import { PushNotification } from "@neb-sports/mysamay-notification-model";

@Injectable()
export class PushRepository {
    db: Db;
    collection: Collection<PushNotification>;

    constructor(private dbProvider: MongoProvider) {}

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(PushNotification.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createPushNotification(push: PushNotification): Promise<PushNotification> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(push);
            if (result.insertedCount === 1) return result.ops[0];
            return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getPushByAnyQuery(
        query: FilterQuery<PushNotification>,
        projection: SchemaMember<PushNotification, any>,
    ): Promise<PushNotification> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countPushByAnyQuery(
        query: FilterQuery<PushNotification>,
    ): Promise<number> {
        try {
            await this.getCollection();
            return await this.collection.countDocuments(query);
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getPushByAnyQueryPaginated(
        query: FilterQuery<PushNotification>,
        skip: number,
        limit: number,
        sort: SortOptionObject<PushNotification>,
        projection: SchemaMember<PushNotification, any>,
    ): Promise<PushNotification[]> {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .skip(skip)
                .limit(limit)
                .sort(sort)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
    }

    async getPushNotificationsByAnyQuery(
        query: FilterQuery<PushNotification>,
        projection: SchemaMember<PushNotification, any>
    ): Promise<PushNotification[]> {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
    }

    async bulkInsert(pushList: PushNotification[]) {
        await this.getCollection();
        let bulk = this.collection.initializeUnorderedBulkOp();
        for(let push of pushList) {
            bulk.insert(push);
        }
        let result = await bulk.execute();
        return result;
    }

   
}
