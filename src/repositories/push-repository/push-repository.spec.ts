import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';

import { PushRepository } from "./push-repository";


describe('PushRepository', () => {
    let provider: PushRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [PushRepository],
        }).compile();

        provider = module.get<PushRepository>(PushRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
