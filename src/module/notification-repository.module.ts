import { PushRecipientRepository } from './../repositories/push-recipient-repository/push-recipient-repository';
import { Module } from '@nestjs/common';
import { DbModule } from '@neb-sports/mysamay-db-provider';



import { PushRepository } from '../repositories/push-repository/push-repository';

@Module({
    imports: [DbModule],
    providers: [PushRepository, PushRecipientRepository],
    exports: [PushRepository, PushRecipientRepository],
})
export class NotificationRepositoryModule {}
